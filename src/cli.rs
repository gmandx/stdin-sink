use clap::{crate_description, crate_name, crate_version, App, AppSettings, Arg, Error};

/// Simple struct to hold all arguments captured from the command line
#[derive(Debug)]
pub struct Arguments {
    /// The editor command to launch with the captured data. If `None` the editor should be looked up from the environment.
    pub editor: Option<String>,
    /// Indicates if the input shouldn't be pre-processed. Currently the only pre-processing made is stripping ANSI escape codes.
    pub raw: bool,
    /// Additional arguments to pass to the editor command. The temporary file is always set as the last argument.
    pub editor_args: Option<Vec<String>>,
    /// Indicates if the captured input should be echoed back to the standard output.
    pub echo: bool,
}

impl Arguments {
    /// Returns a new instance of `Arguments`, inspecting the arguments given to this process.
    /// An error is returned if for any reason parsing fails.
    pub fn get() -> Result<Self, Error> {
        let app = App::new(crate_name!())
            .setting(AppSettings::ColorAuto)
            .setting(AppSettings::ColoredHelp)
            .setting(AppSettings::DeriveDisplayOrder)
            .setting(AppSettings::UnifiedHelpMessage)
            .version(crate_version!())
            .about(crate_description!())
            .setting(AppSettings::TrailingVarArg)
            .arg(
                Arg::with_name("raw")
                    .short("r")
                    .long("raw")
                    .help("Do not strip ANSI escape codes; save the output as-is.")
                    .takes_value(false)
                    .required(false),
            )
            .arg(
                Arg::with_name("echo")
                    .short("e")
                    .long("echo")
                    .help("Echo STDIN data back to STDOUT")
                    .takes_value(false)
                    .required(false),
            )
            .arg(
                Arg::with_name("editor")
                    .value_name("EDITOR")
                    .multiple(false)
                    .takes_value(true)
                    .help("Command to launch with the temporary file")
                    .required(false),
            )
            .arg(Arg::from_usage("<args>... 'Extra arguments to pass to EDITOR. These will always go *before* the filename argument'").required(false));

        let matches = app.get_matches_safe()?;

        Ok(Self {
            editor: matches.value_of("editor").map(String::from),
            raw: matches.is_present("raw"),
            echo: matches.is_present("echo"),
            editor_args: matches
                .values_of("args")
                .map(|values| values.map(String::from).collect()),
        })
    }
}
