use std::borrow::Cow;

use lazy_static::lazy_static;
use regex::bytes::Regex;

lazy_static! {
    static ref CODES: Regex = Regex::new(r"(?-u:[\x1B\x9B][\[\]()#;?]*(?:(?:(?:[a-zA-Z\d]*(?:;[a-zA-Z\d]*)*)?\x07)|(?:(?:\d{1,4}(?:;\d{0,4})*)?[\dA-PR-TZcf-ntqry=><~])))").unwrap();
    static ref EMPTY: Vec<u8> = Vec::new();
}

/// Simple helper function to strip ANSI codes out of a byte slice
///
/// # Example
///
/// ```
/// assert_eq!(
///     strip_ansi_codes(b"\x1B[0;33;49;3;9;4mfoo\x1B[0m").deref(),
///     b"foo",
/// );
/// ```
pub fn strip_ansi_codes(text: &[u8]) -> Cow<'_, [u8]> {
    CODES.replace_all(text, EMPTY.as_slice())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ops::Deref;

    #[test]
    fn test_strip_color_from_string() {
        assert_eq!(
            strip_ansi_codes(
                b"\x1B[0m\x1B[4m\x1B[42m\x1B[31mfoo\x1B[39m\x1B[49m\x1B[24mfoo\x1B[0m"
            )
            .deref(),
            b"foofoo",
        );
    }

    #[test]
    fn test_strip_color_from_ls_command() {
        assert_eq!(
            strip_ansi_codes(b"\x1B[00;38;5;244m\x1B[m\x1B[00;38;5;33mfoo\x1B[0m").deref(),
            b"foo",
        );
    }

    #[test]
    fn test_strip_reset_setfg_setbg_italics_strike_underline_sequence_from_string() {
        assert_eq!(
            strip_ansi_codes(b"\x1B[0;33;49;3;9;4mbar\x1B[0m").deref(),
            b"bar"
        );
    }

    mod tests_from_strip_ansi {
        use super::*;

        #[test]
        fn test_match_ansi_code_in_a_string() {
            assert!(CODES.is_match(b"foo\x1B[4mcake\x1B[0m"));
            assert!(CODES.is_match(b"\x1B[4mcake\x1B[0m"));
            assert!(CODES.is_match(b"foo\x1B[4mcake\x1B[0m"));
            assert!(CODES
                .is_match(b"\x1B[0m\x1B[4m\x1B[42m\x1B[31mfoo\x1B[39m\x1B[49m\x1B[24mfoo\x1B[0m"));
            assert!(CODES.is_match(b"foo\x1B[mfoo"));
        }

        #[test]
        fn test_match_ansi_code_from_ls_command() {
            assert!(CODES.is_match(b"\x1B[00;38;5;244m\x1B[m\x1B[00;38;5;33mfoo\x1B[0m"));
        }

        #[test]
        fn test_match_reset_setfg_setbg_italics_strike_underline_sequence_in_a_string() {
            assert!(CODES.is_match(b"\x1B[0;33;49;3;9;4mbar\x1B[0m"));
            assert_eq!(
                CODES.find(b"foo\x1B[0;33;49;3;9;4mbar").unwrap().as_bytes(),
                b"\x1B[0;33;49;3;9;4m"
            );
        }

        #[test]
        fn test_match_clear_tabs_sequence_in_a_string() {
            assert!(CODES.is_match(b"foo\x1B[0gbar"));
            assert_eq!(CODES.find(b"foo\x1B[0gbar").unwrap().as_bytes(), b"\x1B[0g");
        }

        #[test]
        fn test_match_clear_line_from_cursor_right_in_a_string() {
            assert!(CODES.is_match(b"foo\x1B[Kbar"));
            assert_eq!(CODES.find(b"foo\x1B[Kbar").unwrap().as_bytes(), b"\x1B[K");
        }

        #[test]
        fn test_match_clear_screen_in_a_string() {
            assert!(CODES.is_match(b"foo\x1B[2Jbar"));
            assert_eq!(CODES.find(b"foo\x1B[2Jbar").unwrap().as_bytes(), b"\x1B[2J");
        }
    }
}
