use std::env::{var as env_var, VarError};

fn dissallow_empty(s: String) -> Result<String, VarError> {
    if s.is_empty() {
        Err(VarError::NotPresent)
    } else {
        Ok(s)
    }
}

/// Looks for an editor to run, using the current process' environment.
/// Works a bit like how Git looks up for an editor to launch when
/// editing a commit message, except that only environment variables
/// are inspected.
pub fn lookup_editor() -> String {
    env_var("VISUAL")
        .and_then(dissallow_empty)
        .or_else(|_| env_var("EDITOR"))
        .and_then(dissallow_empty)
        .unwrap_or_else(|_| "vi".into())
}
