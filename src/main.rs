mod ansi_strip;
mod cli;
mod editor;

use std::fs::File;
use std::io::{stdin, stdout, BufRead, BufWriter, Write};
use std::ops::Deref;
use std::process::Command;

use failure::{bail, Error};
use tempfile::NamedTempFile;

pub use crate::ansi_strip::strip_ansi_codes;
pub use crate::cli::Arguments;
pub use crate::editor::lookup_editor;

fn main() -> Result<(), Error> {
    let args = match Arguments::get() {
        Ok(args) => args,
        Err(error) => error.exit(),
    };

    let temp_file = NamedTempFile::new()?;
    let temp_path = temp_file.path().to_path_buf();
    let mut writer = BufWriter::new(temp_file);

    {
        let stream_in = stdin();
        let stream_out = stdout();

        let mut locked_stream_out = if args.echo {
            Some(stream_out.lock())
        } else {
            None
        };

        for line in stream_in.lock().lines() {
            let line = line?;
            let bytes = line.as_bytes();

            if args.raw {
                writer.write_all(bytes)?;
            } else {
                writer.write_all(strip_ansi_codes(bytes).deref())?;
            }

            writer.write_all(b"\n")?;

            if let Some(ref mut out) = locked_stream_out {
                out.write_all(bytes)?;
                out.write_all(b"\n")?;
            }
        }

        if let Some(ref mut out) = locked_stream_out {
            out.flush()?;
        }
    }

    writer.flush()?;

    let editor = args.editor.map(String::from).unwrap_or_else(lookup_editor);
    let editor_status = Command::new(&editor)
        .args(args.editor_args.unwrap_or_else(Vec::new))
        .arg(&temp_path)
        .stdin(File::open("/dev/tty")?)
        .status()?;

    if !editor_status.success() {
        bail!(
            "Command exited with non-zero ({:?}) status",
            editor_status.code()
        );
    }

    Ok(())
}
