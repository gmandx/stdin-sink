# TODO:
* Strip ANSI escape codes (TEST)
* Switch to not strip ANSI escape codes (TEST)
* Arguments are passed to $EDITOR (TEST)
* Eco input back to stdout (TEST)
* Check for placeholder `{}` and stick the filepath argument in there
* Infer $EDITOR with fallback
* Catch signal to stop STDIN collection, flush and show contents until that point
